﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Zone
    {
        public class ZoneLocation {
            public int id { get; internal set; }
            public string name { get; internal set; }

            public ZoneLocation(JObject JLocation)
            {
                this.id = int.Parse(JLocation["id"].ToString());
                this.name = JLocation["name"].ToString();
            }
        }

        public int id { get; internal set; }
        public string name { get; internal set; }
        public string urlSlug { get; internal set; }
        public string description { get; internal set; }
        public ZoneLocation location { get; internal set; }
        public int expansionId { get; internal set; }
        public int numPlayers { get; internal set; }
        public bool isDungeon { get; internal set; }
        public bool isRaid { get; internal set; }
        public int advisedMinLevel { get; internal set; }
        public int advisedMaxLevel { get; internal set; }
        public int advisedHeroicMinLevel { get; internal set; }
        public int advisedHeroicMaxLevel { get; internal set; }
        public List<string> availableModes { get; internal set; }
        public int lfgNormalMinGearLevel { get; internal set; }
        public int lfgHeroicMinGearLevel { get; internal set; }
        public int floors { get; internal set; }
        public List<Boss> bosses { get; internal set; }

        public Zone(JObject rawJson)
        {
            this.id = int.Parse(rawJson[""].ToString());
            this.name = rawJson[""].ToString();
            this.urlSlug = rawJson[""].ToString();
            this.description = rawJson[""].ToString();
            this.location = new ZoneLocation(JObject.Parse(rawJson[""].ToString()));
            this.expansionId = int.Parse(rawJson[""].ToString());
            this.numPlayers = int.Parse(rawJson[""].ToString());
            this.isDungeon = bool.Parse(rawJson[""].ToString());
            this.isRaid = bool.Parse(rawJson[""].ToString());
            this.advisedMinLevel = int.Parse(rawJson[""].ToString());
            this.advisedMaxLevel = int.Parse(rawJson[""].ToString());
            this.advisedHeroicMinLevel = int.Parse(rawJson[""].ToString());
            this.advisedHeroicMaxLevel = int.Parse(rawJson[""].ToString());
            this.availableModes = new List<string>();
            foreach (string JAvailableMode in rawJson[""])
                this.availableModes.Add(JAvailableMode);
            this.lfgHeroicMinGearLevel = int.Parse(rawJson[""].ToString());
            this.lfgNormalMinGearLevel = int.Parse(rawJson[""].ToString());
            this.floors = int.Parse(rawJson[""].ToString());
            this.bosses = new List<Boss>();
            foreach (JObject JBoss in rawJson["bosses"])
                this.bosses.Add(new Boss(JBoss));
        }
    }
}
