﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Auction
    {
        public class AuctionBonus
        {
            public int bonusListId { get; internal set; }

            public AuctionBonus(JObject JBonus)
            {
                this.bonusListId = int.Parse(JBonus["bonusListId"].ToString());
            }
        }
        public class AuctionModifier
        {
            public int type { get; internal set; }
            public long value { get; internal set; }
            
            public AuctionModifier(JObject JModifier)
            {
                this.type = int.Parse(JModifier["type"].ToString());
                this.value = long.Parse(JModifier["value"].ToString());
            }
        }

        public long auc { get; internal set; }
        public int item { get; internal set; }
        public string owner { get; internal set; }
        public string ownerRealm { get; internal set; }
        public long bid { get; internal set; }
        public long buyout { get; internal set; }
        public int quantity { get; internal set; }
        public string timeLeft { get; internal set; }
        public int rand { get; internal set; }
        public long seed { get; internal set; }
        public int context { get; internal set; }
        public List<AuctionBonus> bonusLists { get; internal set; }
        public List<AuctionModifier> modifiers { get; internal set; }
        public int petSpeciesId { get; internal set; }
        public int petBreedId { get; internal set; }
        public int petLevel { get; internal set; }
        public int petQualityId { get; internal set; }

        public Auction(JObject rawJson)
        {
            if (rawJson["auc"] != null)
                this.auc = long.Parse(rawJson["auc"].ToString());
            if (rawJson["item"] != null)
                this.item = int.Parse(rawJson["item"].ToString());
            if (rawJson["owner"] != null)
                this.owner = rawJson["owner"].ToString();
            if (rawJson["ownerRealm"] != null)
                this.ownerRealm = rawJson["ownerRealm"].ToString();
            if (rawJson["bid"] != null)
                this.bid = long.Parse(rawJson["bid"].ToString());
            if (rawJson["buyout"] != null)
                this.buyout = long.Parse(rawJson["buyout"].ToString());
            if (rawJson["quantity"] != null)
                this.quantity = int.Parse(rawJson["quantity"].ToString());
            if (rawJson["timeLeft"] != null)
                this.timeLeft = rawJson["timeLeft"].ToString();
            if (rawJson["rand"] != null)
                this.rand = int.Parse(rawJson["rand"].ToString());
            if (rawJson["seed"] != null)
                this.seed = long.Parse(rawJson["seed"].ToString());
            if (rawJson["context"] != null)
                this.context = int.Parse(rawJson["context"].ToString());
            if (rawJson["bonusLists"] != null && rawJson["bonusLists"].HasValues)
            {
                this.bonusLists = new List<AuctionBonus>();
                foreach (JObject JBonus in rawJson["bonusLists"])
                    this.bonusLists.Add(new AuctionBonus(JBonus));
            }
            if (rawJson["modifiers"] != null && rawJson["modifiers"].HasValues)
            {
                this.modifiers = new List<AuctionModifier>();
                foreach (JObject JModifier in rawJson["modifiers"])
                    this.modifiers.Add(new AuctionModifier(JModifier));
            }
            if (rawJson["petSpeciesId"] != null)
                this.petSpeciesId = int.Parse(rawJson["petSpeciesId"].ToString());
            if (rawJson["petBreedId"] != null)
                this.petBreedId = int.Parse(rawJson["petBreedId"].ToString());
            if (rawJson["petLevel"] != null)
                this.petLevel = int.Parse(rawJson["petLevel"].ToString());
            if (rawJson["petQualityId"] != null)
                this.petQualityId = int.Parse(rawJson["petQualityId"].ToString());
        }
    }
}
