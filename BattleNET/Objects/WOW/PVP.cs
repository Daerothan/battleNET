﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class PVP
    {
        public int ranking { get; internal set; }
        public int rating { get; internal set; }
        public string name { get; internal set; }
        public int realmId { get; internal set; }
        public string realmName { get; internal set; }
        public string realmSlug { get; internal set; }
        public int raceId { get; internal set; }
        public int classId { get; internal set; }
        public int specId { get; internal set; }
        public int factionId { get; internal set; }
        public int genderId { get; internal set; }
        public int seasonWins { get; internal set; }
        public int seasonLosses { get; internal set; }
        public int weeklyWins { get; internal set; }
        public int weeklyLosses { get; internal set; }

        public PVP(JObject rawJson)
        {
            this.ranking = int.Parse(rawJson["ranking"].ToString());
            this.rating = int.Parse(rawJson["rating"].ToString());
            this.name = rawJson["name"].ToString();
            this.realmId = int.Parse(rawJson["realmId"].ToString());
            this.realmName = rawJson["realmName"].ToString();
            this.realmSlug = rawJson["realmSlug"].ToString();
            this.raceId = int.Parse(rawJson["raceId"].ToString());
            this.classId = int.Parse(rawJson["classId"].ToString());
            this.specId = int.Parse(rawJson["specId"].ToString());
            this.factionId = int.Parse(rawJson["factionId"].ToString());
            this.genderId = int.Parse(rawJson["genderId"].ToString());
            this.seasonWins = int.Parse(rawJson["seasonWins"].ToString());
            this.seasonLosses = int.Parse(rawJson["seasonLosses"].ToString());
            this.weeklyWins = int.Parse(rawJson["weeklyWins"].ToString());
            this.weeklyLosses = int.Parse(rawJson["weeklyLosses"].ToString());
        }
    }
}
