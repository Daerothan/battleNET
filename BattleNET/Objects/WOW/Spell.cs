﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Spell
    {
        public int id { get; internal set; }
        public string name { get; internal set; }
        public string icon { get; internal set; }
        public string description { get; internal set; }
        public string range { get; internal set; }
        public string powerCost { get; internal set; }
        public string castTime { get; internal set; }
        public string cooldown { get; internal set; }

        public Spell(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.name = rawJson["name"].ToString();
            this.icon = rawJson["icon"].ToString();
            this.description = rawJson["description"].ToString();
            this.range = rawJson["range"].ToString();
            this.powerCost = rawJson["powerCost"].ToString();
            this.castTime = rawJson["castTime"].ToString();
            this.cooldown = rawJson["cooldown"].ToString();
        }
    }
}
