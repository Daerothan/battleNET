﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Boss
    {
        public class BossNpc
        {
            public int id { get; internal set; }
            public string name { get; internal set; }
            public string urlSlug { get; internal set; }

            public BossNpc(JToken JNpc)
            {
                if (JNpc["id"] != null)
                    this.id = int.Parse(JNpc["id"].ToString());
                if (JNpc["name"] != null)
                    this.name = JNpc["name"].ToString();
                if (JNpc["urlSlug"] != null)
                    this.urlSlug = JNpc["urlSlug"].ToString();
            }
        }

        public int id { get; internal set; }
        public string name { get; internal set; }
        public string urlSlug { get; internal set; }
        public string description { get; internal set; }
        public int zoneId { get; internal set; }
        public bool availableInNormalMode { get; internal set; }
        public bool availableInHeroicMode { get; internal set; }
        public int health { get; internal set; }
        public int heroicHealth { get; internal set; }
        public int level { get; internal set; }
        public int heroicLevel { get; internal set; }
        public int journalId { get; internal set; }
        public List<BossNpc> npcs { get; internal set; }

        public Boss(JObject rawJson)
        {
            if (rawJson["id"] != null)
                this.id = int.Parse(rawJson["id"].ToString());
            if (rawJson["name"] != null)
                this.name = rawJson["name"].ToString();
            if (rawJson["urlSlug"] != null)
                this.urlSlug = rawJson["urlSlug"].ToString();
            if (rawJson["description"] != null)
                this.description = rawJson["description"].ToString();
            if (rawJson["zoneId"] != null)
                this.zoneId = int.Parse(rawJson["zoneId"].ToString());
            if (rawJson["availableInNormalMode"] != null)
                this.availableInNormalMode = bool.Parse(rawJson["availableInNormalMode"].ToString());
            if (rawJson["availableInHeroicMode"] != null)
                this.availableInHeroicMode = bool.Parse(rawJson["availableInHeroicMode"].ToString());
            if (rawJson["health"] != null)
                this.health = int.Parse(rawJson["health"].ToString());
            if (rawJson["heroicHealth"] != null)
                this.heroicHealth = int.Parse(rawJson["heroicHealth"].ToString());
            if (rawJson["level"] != null)
                this.level = int.Parse(rawJson["level"].ToString());
            if (rawJson["heroicLevel"] != null)
                this.heroicLevel = int.Parse(rawJson["heroicLevel"].ToString());
            if (rawJson["journalId"] != null)
                this.journalId = int.Parse(rawJson["journalId"].ToString());
            if (rawJson["npcs"] != null)
            {
                this.npcs = new List<BossNpc>();
                foreach (JToken JNpc in rawJson["npcs"])
                {
                    BossNpc npc = new BossNpc(JNpc);
                    this.npcs.Add(npc);
                }
            }
        }
    }
}
