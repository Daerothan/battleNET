﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Battlegroup
    {
        public string name { get; internal set; }
        public string slug { get; internal set; }

        public Battlegroup(JObject rawJson)
        {
            this.name = rawJson["name"].ToString();
            this.slug = rawJson["slug"].ToString();
        }
    }
}
