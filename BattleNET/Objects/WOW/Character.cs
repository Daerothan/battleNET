﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Character
    {
        public class CharacterAchievements
        {
            public List<int> achievementsCompleted { get; internal set; }
            public List<long> achievementsCompletedTimestamp { get; internal set; }
            public List<int> criteria { get; internal set; }
            public List<int> criteriaQuantity { get; internal set; }
            public List<long> criteriaTimestamp { get; internal set; }
            public List<long> criteriaCreated { get; internal set; }

            public CharacterAchievements(JToken JAchievements)
            {
                if (JAchievements["achievementsCompleted"] != null && JAchievements["achievementsCompleted"].HasValues)
                {
                    achievementsCompleted = new List<int>();
                    foreach (int JAchievementComplete in JAchievements["achievementsCompleted"])
                    {
                        achievementsCompleted.Add(JAchievementComplete);
                    }
                }

                if (JAchievements["achievementsCompletedTimestamp"] != null && JAchievements["achievementsCompletedTimestamp"].HasValues)
                {
                    achievementsCompletedTimestamp = new List<long>();
                    foreach (long achievementsCompleteTimestamp in JAchievements["achievementsCompletedTimestamp"])
                    {
                        achievementsCompletedTimestamp.Add(achievementsCompleteTimestamp);
                    }
                }

                if (JAchievements["criteria"] != null && JAchievements["criteria"].HasValues)
                {
                    criteria = new List<int>();
                    foreach (int JCriteria in JAchievements["criteria"])
                    {
                        criteria.Add(JCriteria);
                    }
                }

                if (JAchievements["criteriaQuantity"] != null && JAchievements["criteriaQuantity"].HasValues)
                {
                    criteriaQuantity = new List<int>();
                    foreach (int JCriteriaQuantity in JAchievements["criteriaQuantity"])
                    {
                        criteriaQuantity.Add(JCriteriaQuantity);
                    }
                }

                if (JAchievements["criteriaTimestamp"] != null && JAchievements["criteriaTimestamp"].HasValues)
                {
                    criteriaTimestamp = new List<long>();
                    foreach (long JCriteriaTimestamp in JAchievements["criteriaTimestamp"])
                    {
                        criteriaTimestamp.Add(JCriteriaTimestamp);
                    }
                }

                if (JAchievements["criteriaCreated"] != null && JAchievements["criteriaCreated"].HasValues)
                {
                    criteriaCreated = new List<long>();
                    foreach (long JCriteriaCreated in JAchievements["criteriaCreated"])
                    {
                        criteriaCreated.Add(JCriteriaCreated);
                    }
                }
            }
        }
        public class CharacterAppearance
        {
            public int faceVariation { get; internal set; }
            public int skinColor { get; internal set; }
            public int hairVariation { get; internal set; }
            public int hairColor { get; internal set; }
            public int featureVariation { get; internal set; }
            public bool showHelm { get; internal set; }
            public bool showCloak { get; internal set; }

            public CharacterAppearance(JToken JAppearance)
            {
                if (JAppearance["faceVariation"] != null)
                    this.faceVariation = int.Parse(JAppearance["faceVariation"].ToString());
                if (JAppearance["skinColor"] != null)
                    this.skinColor = int.Parse(JAppearance["skinColor"].ToString());
                if (JAppearance["hairVariation"] != null)
                    this.hairVariation = int.Parse(JAppearance["hairVariation"].ToString());
                if (JAppearance["hairColor"] != null)
                    this.hairColor = int.Parse(JAppearance["hairColor"].ToString());
                if (JAppearance["featureVariation"] != null)
                    this.featureVariation = int.Parse(JAppearance["featureVariation"].ToString());
                if (JAppearance["showHelm"] != null)
                    this.showHelm = bool.Parse(JAppearance["showHelm"].ToString());
                if (JAppearance["showCloak"] != null)
                    this.showCloak = bool.Parse(JAppearance["showCloak"].ToString());
            }
        }
        public class CharacterFeed
        {
            public string type { get; internal set; }
            public long timestamp { get; internal set; }
            public int itemId { get; internal set; }
            public string context { get; internal set; }
            public List<int> bonusLists { get; internal set; }
            public Achievement achievement { get; internal set; }
            public bool featOfStrength { get; internal set; }
            public Achievement criteria { get; internal set; }
            public int quantity { get; internal set; }
            public string name { get; internal set; }

            public CharacterFeed(JToken JFeed)
            {
                if (JFeed["type"] != null)
                    this.type = JFeed["type"].ToString();
                if (JFeed["timestamp"] != null)
                    this.timestamp = long.Parse(JFeed["timestamp"].ToString());
                if (JFeed["itemId"] != null)
                    this.itemId = int.Parse(JFeed["itemId"].ToString());
                if (JFeed["context"] != null)
                    this.context = JFeed["context"].ToString();
                if (JFeed["bonusLists"] != null && JFeed["bonusLists"].HasValues)
                {
                    this.bonusLists = new List<int>();
                    foreach (int JBonus in JFeed["bonusLists"])
                    {
                        this.bonusLists.Add(JBonus);
                    }
                }
                if (JFeed["achievement"] != null && JFeed["achievement"].HasValues)
                    this.achievement = new Achievement(JObject.Parse(JFeed["achievement"].ToString()));
                if (JFeed["featOfStrength"] != null)
                    this.featOfStrength = bool.Parse(JFeed["featOfStrength"].ToString());
                if (JFeed["context"] != null)
                    this.criteria = new Achievement(JObject.Parse(JFeed["criteria"].ToString()));
                if (JFeed["quantity"] != null)
                    this.quantity = int.Parse(JFeed["quantity"].ToString());
                if (JFeed["name"] != null)
                    this.name = JFeed["name"].ToString();
            }
        }
        public class CharacterSpec
        {
            public string name { get; internal set; }
            public string role { get; internal set; }
            public string backgroundImage { get; internal set; }
            public string icon { get; internal set; }
            public string description { get; internal set; }
            public int order { get; internal set; }

            public CharacterSpec(JObject JSpec)
            {
                if (JSpec["name"] != null)
                    this.name = JSpec["name"].ToString();
                if (JSpec["role"] != null)
                    this.role = JSpec["role"].ToString();
                if (JSpec["backgroundImage"] != null)
                    this.backgroundImage = JSpec["backgroundImage"].ToString();
                if (JSpec["icon"] != null)
                    this.icon = JSpec["icon"].ToString();
                if (JSpec["description"] != null)
                    this.description = JSpec["description"].ToString();
                if (JSpec["order"] != null)
                    this.order = int.Parse(JSpec["order"].ToString());
            }
        }

        public long lastModified { get; internal set; }
        public string name { get; internal set; }
        public string realm { get; internal set; }
        public string battlegroup { get; internal set; }
        [JsonProperty("class")]
        public int classId { get; internal set; }
        public int race { get; internal set; }
        public int gender { get; internal set; }
        public int level { get; internal set; }
        public int achievementPoints { get; internal set; }
        public string thumbnail { get; internal set; }
        public CharacterSpec spec { get; internal set; }
        public string guild { get; internal set; }
        public string guildRealm { get; internal set; }
        public string calcClass { get; internal set; }
        public int faction { get; internal set; }
        public CharacterAchievements achievements { get; internal set; }
        public CharacterAppearance appearance { get; internal set; }
        public List<CharacterFeed> feed { get; internal set; }

        public int totalHonorableKills { get; internal set; }

        public Character(JObject rawJson)
        {
            if (rawJson["lastModified"] != null)
                this.lastModified = long.Parse(rawJson["lastModified"].ToString());
            if (rawJson["name"] != null)
                this.name = rawJson["name"].ToString();
            if (rawJson["realm"] != null)
                this.realm = rawJson["realm"].ToString();
            if (rawJson["battlegroup"] != null)
                this.battlegroup = rawJson["battlegroup"].ToString();
            if (rawJson["class"] != null)
                this.classId = int.Parse(rawJson["class"].ToString());
            if (rawJson["race"] != null)
                this.race = int.Parse(rawJson["race"].ToString());
            if (rawJson["gender"] != null)
                this.gender = int.Parse(rawJson["gender"].ToString());
            if (rawJson["level"] != null)
                this.level = int.Parse(rawJson["level"].ToString());
            if (rawJson["achievementPoints"] != null)
                this.achievementPoints = int.Parse(rawJson["achievementPoints"].ToString());
            if (rawJson["thumbnail"] != null)
                this.thumbnail = rawJson["thumbnail"].ToString();
            if (rawJson["guild"] != null)
                this.guild = rawJson["guild"].ToString();
            if (rawJson["guildRealm"] != null)
                this.guildRealm = rawJson["guildRealm"].ToString();
            if (rawJson["calcClass"] != null)
                this.calcClass = rawJson["calcClass"].ToString();
            if (rawJson["faction"] != null)
                this.faction = int.Parse(rawJson["faction"].ToString());
            if (rawJson["achievements"] != null && rawJson["achievements"].HasValues)
                this.achievements = new CharacterAchievements(rawJson["achievements"]);
            if (rawJson["appearance"] != null && rawJson["appearance"].HasValues)
                this.appearance = new CharacterAppearance(rawJson["appearance"]);
            if (rawJson["feed"] != null && rawJson["feed"].HasValues)
            {
                this.feed = new List<CharacterFeed>();
                foreach (JObject JFeed in rawJson["feed"])
                {
                    CharacterFeed characterFee = new CharacterFeed(JFeed);
                    this.feed.Add(characterFee);
                }
            }
        }
    }
}
