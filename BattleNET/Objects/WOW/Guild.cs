﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Guild
    {
        public class GuildMember
        {
            public Character character { get; internal set; }
            public int rank { get; internal set; }

            public GuildMember(JObject JMember)
            {
                this.character = new Character(JObject.Parse(JMember["character"].ToString()));
                this.rank = int.Parse(JMember["rank"].ToString());
            }
        }
        public class GuildAchievements
        {
            public List<long> achievementsCompleted { get; internal set; }
            public List<long> achievementsCompletedTimestamp { get; internal set; }
            public List<long> criteria { get; internal set; }
            public List<long> criteriaQuantity { get; internal set; }
            public List<long> criteriaTimestamp { get; internal set; }
            public List<long> criteriaCreated { get; internal set; }

            public GuildAchievements(JObject JAchievements)
            {
                if (JAchievements["achievementsCompleted"] != null && JAchievements["achievementsCompleted"].HasValues)
                {
                    this.achievementsCompleted = new List<long>();
                    foreach (long JAchievementComplete in JAchievements["achievementsCompleted"])
                    {
                        this.achievementsCompleted.Add(JAchievementComplete);
                    }
                }

                if (JAchievements["achievementsCompletedTimestamp"] != null && JAchievements["achievementsCompletedTimestamp"].HasValues)
                {
                    this.achievementsCompletedTimestamp = new List<long>();
                    foreach (long achievementsCompleteTimestamp in JAchievements["achievementsCompletedTimestamp"])
                    {
                        this.achievementsCompletedTimestamp.Add(achievementsCompleteTimestamp);
                    }
                }

                if (JAchievements["criteria"] != null && JAchievements["criteria"].HasValues)
                {
                    this.criteria = new List<long>();
                    foreach (long JCriteria in JAchievements["criteria"])
                    {
                        this.criteria.Add(JCriteria);
                    }
                }

                if (JAchievements["criteriaQuantity"] != null && JAchievements["criteriaQuantity"].HasValues)
                {
                    this.criteriaQuantity = new List<long>();
                    foreach (long JCriteriaQuantity in JAchievements["criteriaQuantity"])
                    {
                        this.criteriaQuantity.Add(JCriteriaQuantity);
                    }
                }

                if (JAchievements["criteriaTimestamp"] != null && JAchievements["criteriaTimestamp"].HasValues)
                {
                    this.criteriaTimestamp = new List<long>();
                    foreach (long JCriteriaTimestamp in JAchievements["criteriaTimestamp"])
                    {
                        this.criteriaTimestamp.Add(JCriteriaTimestamp);
                    }
                }

                if (JAchievements["criteriaCreated"] != null && JAchievements["criteriaCreated"].HasValues)
                {
                    this.criteriaCreated = new List<long>();
                    foreach (long JCriteriaCreated in JAchievements["criteriaCreated"])
                    {
                        this.criteriaCreated.Add(JCriteriaCreated);
                    }
                }
            }
        }
        public class GuildNew
        {
            public string type { get; internal set; }
            public string character { get; internal set; }
            public long timestamp { get; internal set; }
            public int itemId { get; internal set; }
            public string context { get; internal set; }
            public List<int> bonusLists { get; internal set; }
            public Achievement achievement { get; internal set; }

            public GuildNew(JObject JNew)
            {
                if (JNew["type"] != null)
                    this.type = JNew["type"].ToString();
                if (JNew["character"] != null)
                    this.character = JNew["character"].ToString();
                if (JNew["timestamp"] != null)
                    this.timestamp = long.Parse(JNew["timestamp"].ToString());
                if (JNew["itemId"] != null)
                    this.itemId = int.Parse(JNew["itemId"].ToString());
                if (JNew["context"] != null)
                    this.context = JNew["context"].ToString();
                if (JNew["bonusLists"] != null && JNew["bonusLists"].HasValues)
                {
                    this.bonusLists = new List<int>();
                    foreach (int JBonus in JNew["bonusLists"])
                    {
                        this.bonusLists.Add(JBonus);
                    }
                }
                if (JNew["achievement"] != null)
                    this.achievement = new Achievement(JObject.Parse(JNew["achievement"].ToString()));
            }
        }

        public class GuildEmblem
        {
            public int icon { get; internal set; }
            [JsonProperty("iconColor")]
            public string iconColorRaw { get; internal set; }
            [JsonIgnore]
            public Color iconColor { get; internal set; }
            public int border { get; internal set; }
            [JsonProperty("borderColor")]
            public string borderColorRaw { get; internal set; }
            [JsonIgnore]
            public Color borderColor { get; internal set; }
            [JsonProperty("backgroundColor")]
            public string backgroundColorRaw { get; internal set; }
            [JsonIgnore]
            public Color backgroundColor { get; internal set; }

            public GuildEmblem(JToken JEmblem)
            {
                this.icon = int.Parse(JEmblem["icon"].ToString());
                this.iconColorRaw = JEmblem["iconColor"].ToString();
                this.iconColor = ColorTranslator.FromHtml(this.iconColorRaw);
                this.border = int.Parse(JEmblem["border"].ToString());
                this.borderColorRaw = JEmblem["borderColor"].ToString();
                this.borderColor = ColorTranslator.FromHtml(this.borderColorRaw);
                this.backgroundColorRaw = JEmblem["backgroundColor"].ToString();
                this.backgroundColor = ColorTranslator.FromHtml(this.backgroundColorRaw);
            }
        }

        public string name { get; internal set; }
        public string realm { get; internal set; }
        public string battlegroup { get; internal set; }
        public int level { get; internal set; }
        public int side { get; internal set; }
        public int achievementPoints { get; internal set; }
        public List<GuildMember> members { get; internal set; }
        public GuildAchievements achievements { get; internal set; }
        public List<GuildNew> news { get; internal set; }
        public List<Challenge> challenge { get; internal set; }

        public GuildEmblem emblem { get; internal set; }

        public Guild(JObject rawJson)
        {
            if (rawJson["name"] != null)
                this.name = rawJson["name"].ToString();
            if (rawJson["realm"] != null)
                this.realm = rawJson["realm"].ToString();
            if (rawJson["battlegroup"] != null)
                this.battlegroup = rawJson["battlegroup"].ToString();
            if (rawJson["level"] != null)
                this.level = int.Parse(rawJson["level"].ToString());
            if (rawJson["side"] != null)
                this.side = int.Parse(rawJson["side"].ToString());
            if (rawJson["achievementPoints"] != null)
                this.achievementPoints = int.Parse(rawJson["achievementPoints"].ToString());
            if (rawJson["members"] != null && rawJson["members"].HasValues)
            {
                this.members = new List<GuildMember>();
                foreach (JObject JMember in rawJson["members"])
                    this.members.Add(new GuildMember(JMember));
            }
            if (rawJson["achievements"] != null)
                this.achievements = new GuildAchievements(JObject.Parse(rawJson["achievements"].ToString()));
            if (rawJson["news"] != null && rawJson["news"].HasValues)
            {
                this.news = new List<GuildNew>();
                foreach (JObject JNew in rawJson["news"])
                    this.news.Add(new GuildNew(JNew));
            }
            if (rawJson["challenge"] != null && rawJson["challenge"].HasValues)
            {
                this.challenge = new List<Challenge>();
                foreach (JObject JChallenge in rawJson["challenge"])
                    this.challenge.Add(new Challenge(JChallenge));
            }
        }
    }
}
