﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Race
    {
        public int id { get; internal set; }
        public int mask { get; internal set; }
        public string side { get; internal set; }
        public string name { get; internal set; }

        public Race(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.mask = int.Parse(rawJson["mask"].ToString());
            this.side = rawJson["side"].ToString();
            this.name = rawJson["name"].ToString();
        }
    }
}
