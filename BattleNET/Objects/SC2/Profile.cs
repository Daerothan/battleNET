﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.SC2
{
    public class Profile
    {
        public class ProfileCareer
        {
            public string primaryRace { get; internal set; }
            public int terranWins { get; internal set; }
            public int protossWins { get; internal set; }
            public int zergWins { get; internal set; }
            public string highest1v1Rank { get; internal set; }
            public string highestTeamRank { get; internal set; }
            public int seasonTotalGames { get; internal set; }
            public int careerTotalGames { get; internal set; }

            public ProfileCareer(JObject JCareer)
            {
                this.primaryRace = JCareer["primaryRace"].ToString();
                this.terranWins = int.Parse(JCareer["terranWins"].ToString());
                this.protossWins = int.Parse(JCareer["protossWins"].ToString());
                this.zergWins = int.Parse(JCareer["zergWins"].ToString());
                this.highest1v1Rank = JCareer["highest1v1Rank"].ToString();
                this.highestTeamRank = JCareer["highestTeamRank"].ToString();
                this.seasonTotalGames = int.Parse(JCareer["seasonTotalGames"].ToString());
                this.careerTotalGames = int.Parse(JCareer["careerTotalGames"].ToString());
            }
        }
        public class ProfileSwarmLevels
        {
            public class ProfileSwarmLevelsRace
            {
                public int level { get; internal set; }
                public int totalLevelXP { get; internal set; }
                public int currentLevelXP { get; internal set; }

                public ProfileSwarmLevelsRace(JObject JSwarmLevelsRace)
                {
                    this.level = int.Parse(JSwarmLevelsRace["level"].ToString());
                    this.totalLevelXP = int.Parse(JSwarmLevelsRace["totalLevelXP"].ToString());
                    this.currentLevelXP = int.Parse(JSwarmLevelsRace["currentLevelXP"].ToString());
                }
            }

            public int level { get; internal set; }
            public ProfileSwarmLevelsRace terran { get; internal set; }
            public ProfileSwarmLevelsRace zerg { get; internal set; }
            public ProfileSwarmLevelsRace protoss { get; internal set; }

            public ProfileSwarmLevels(JObject JSwarmLevels)
            {
                this.level = int.Parse(JSwarmLevels["level"].ToString());
                this.terran = new ProfileSwarmLevelsRace(JObject.Parse(JSwarmLevels["terran"].ToString()));
                this.zerg = new ProfileSwarmLevelsRace(JObject.Parse(JSwarmLevels["zerg"].ToString()));
                this.protoss = new ProfileSwarmLevelsRace(JObject.Parse(JSwarmLevels["protoss"].ToString()));
            }
        }
        public class ProfileCampaign
        {
            public string wol { get; internal set; }
            public string hots { get; internal set; }

            public ProfileCampaign(JObject JCampaign)
            {
                this.wol = JCampaign["wol"].ToString();
                this.hots = JCampaign["hots"].ToString();
            }
        }
        public class ProfileSeason
        {
            public int seasonId { get; internal set; }
            public int seasonNumber { get; internal set; }
            public int seasonYear { get; internal set; }
            public int totalGamesThisSeason { get; internal set; }

            public ProfileSeason(JObject JSeason)
            {
                this.seasonId = int.Parse(JSeason["seasonId"].ToString());
                this.seasonNumber = int.Parse(JSeason["seasonNumber"].ToString());
                this.seasonYear = int.Parse(JSeason["seasonYear"].ToString());
                this.totalGamesThisSeason = int.Parse(JSeason["totalGamesThisSeason"].ToString());
            }
        }
        public class ProfileReward
        {
            public List<int> selected { get; internal set; }
            public List<int> earned { get; internal set; }

            public ProfileReward(JObject JReward)
            {
                this.selected = new List<int>();
                this.earned = new List<int>();

                foreach (int JSelected in JReward["selected"])
                    this.selected.Add(JSelected);
                foreach (int JEarned in JReward["earned"])
                    this.earned.Add(JEarned);
            }
        }
        public class ProfileAchievements
        {
            public class ProfileAchievementsPoints
            {
                public class ProfileAchievementsPointsCategoryPoints
                {
                    [JsonProperty("4325382")]
                    public int _4325382 { get; internal set; }
                    [JsonProperty("4330138")]
                    public int _4330138 { get; internal set; }
                    [JsonProperty("4386911")]
                    public int _4386911 { get; internal set; }
                    [JsonProperty("4325379")]
                    public int _4325379 { get; internal set; }
                    [JsonProperty("4325410")]
                    public int _4325410 { get; internal set; }
                    [JsonProperty("4325377")]
                    public int _4325377 { get; internal set; }

                    public ProfileAchievementsPointsCategoryPoints(JObject JAchievementsPointsCategoryPoints)
                    {
                        this._4325382 = int.Parse(JAchievementsPointsCategoryPoints["4325382"].ToString());
                        this._4330138 = int.Parse(JAchievementsPointsCategoryPoints["4330138"].ToString());
                        this._4386911 = int.Parse(JAchievementsPointsCategoryPoints["4386911"].ToString());
                        this._4325379 = int.Parse(JAchievementsPointsCategoryPoints["4325379"].ToString());
                        this._4325410 = int.Parse(JAchievementsPointsCategoryPoints["4325410"].ToString());
                        this._4325377 = int.Parse(JAchievementsPointsCategoryPoints["4325377"].ToString());
                    }
                }

                public int totalPoints { get; internal set; }
                public ProfileAchievementsPointsCategoryPoints categoryPoints { get; internal set; }

                public ProfileAchievementsPoints(JObject AchievementsPoints)
                {
                    this.totalPoints = int.Parse(AchievementsPoints["totalPoints"].ToString());
                    this.categoryPoints = new ProfileAchievementsPointsCategoryPoints(JObject.Parse(AchievementsPoints["categoryPoints"].ToString()));
                }
            }
            public class ProfileAchievementsAchievement
            {
                public long achievementId { get; internal set; }
                public long completionDate { get; internal set; }

                public ProfileAchievementsAchievement(JObject JAchievementsAchievement)
                {
                    this.achievementId = long.Parse(JAchievementsAchievement["achievementId"].ToString());
                    this.completionDate = long.Parse(JAchievementsAchievement["completionDate"].ToString());
                }
            }

            public ProfileAchievementsPoints points { get; internal set; }
            public List<ProfileAchievementsAchievement> achievements { get; internal set; }

            public ProfileAchievements(JObject JAchievements)
            {
                this.achievements = new List<ProfileAchievementsAchievement>();

                this.points = new ProfileAchievementsPoints(JObject.Parse(JAchievements["points"].ToString()));
                foreach (JObject JAchievementsAchievement in JAchievements["achievements"])
                    this.achievements.Add(new ProfileAchievementsAchievement(JAchievementsAchievement));
            }
        }
        public class ProfileLadders
        {
            public class ProfileLaddersSeason
            {
                public class ProfileLaddersSeasonLadder
                {
                    public string ladderName { get; internal set; }
                    public int ladderId { get; internal set; }
                    public int division { get; internal set; }
                    public int rank { get; internal set; }
                    public string league { get; internal set; }
                    public string matchMakingQueue { get; internal set; }
                    public int wins { get; internal set; }
                    public int losses { get; internal set; }
                    public bool showcase { get; internal set; }

                    public ProfileLaddersSeasonLadder(JObject JSeasonLadder)
                    {
                        this.ladderName = JSeasonLadder["ladderName"].ToString();
                        this.ladderId = int.Parse(JSeasonLadder["ladderId"].ToString());
                        this.division = int.Parse(JSeasonLadder["division"].ToString());
                        this.rank = int.Parse(JSeasonLadder["rank"].ToString());
                        this.league = JSeasonLadder["league"].ToString();
                        this.matchMakingQueue = JSeasonLadder["matchMakingQueue"].ToString();
                        this.wins = int.Parse(JSeasonLadder["wins"].ToString());
                        this.losses = int.Parse(JSeasonLadder["losses"].ToString());
                        this.showcase = bool.Parse(JSeasonLadder["showcase"].ToString());
                    }
                }
                public class ProfileLaddersSeasonNonRanked
                {
                    public string mmq { get; internal set; }
                    public int gamesPlayed { get; internal set; }

                    public ProfileLaddersSeasonNonRanked(JObject JSeasonNonRanked)
                    {
                        this.mmq = JSeasonNonRanked["mmq"].ToString();
                        this.gamesPlayed = int.Parse(JSeasonNonRanked["gamesPlayed"].ToString());
                    }
                }

                public List<ProfileLaddersSeasonLadder> ladder { get; internal set; }
                public List<Profile> characters { get; internal set; }
                public List<ProfileLaddersSeasonNonRanked> nonRanked { get; internal set; }

                public ProfileLaddersSeason(JObject JSeason)
                {
                    this.ladder = new List<ProfileLaddersSeasonLadder>();
                    this.characters = new List<Profile>();
                    this.nonRanked = new List<ProfileLaddersSeasonNonRanked>();

                    foreach (JObject JLadder in JSeason["ladder"])
                        this.ladder.Add(new ProfileLaddersSeasonLadder(JLadder));
                    foreach (JObject JCharacter in JSeason["characters"])
                        this.characters.Add(new Profile(JCharacter));
                    foreach (JObject JNonRanked in JSeason["nonRanked"])
                        this.nonRanked.Add(new ProfileLaddersSeasonNonRanked(JNonRanked));
                }
            }

            public List<ProfileLaddersSeason> currentSeason { get; internal set; }
            public List<ProfileLaddersSeason> previousSeason { get; internal set; }
            public List<ProfileLaddersSeason> showcasePlacement { get; internal set; }

            public ProfileLadders(JObject JLadders)
            {
                this.currentSeason = new List<ProfileLaddersSeason>();
                this.previousSeason = new List<ProfileLaddersSeason>();
                this.showcasePlacement = new List<ProfileLaddersSeason>();

                foreach (JObject JCurrentSeason in JLadders["currentSeason"])
                    this.currentSeason.Add(new ProfileLaddersSeason(JCurrentSeason));
                foreach (JObject JPreviousSeason in JLadders["previousSeason"])
                    this.previousSeason.Add(new ProfileLaddersSeason(JPreviousSeason));
                foreach (JObject JShowcasePlacement in JLadders["showcasePlacement"])
                    this.showcasePlacement.Add(new ProfileLaddersSeason(JShowcasePlacement));
            }
        }

        public int id { get; internal set; }
        public int realm { get; internal set; }
        public string displayName { get; internal set; }
        public string clanName { get; internal set; }
        public string clanTag { get; internal set; }
        public string profilePath { get; internal set; }
        public Icon portrait { get; internal set; }
        public ProfileCareer career { get; internal set; }
        public ProfileSwarmLevels swarmLevels { get; internal set; }
        public ProfileCampaign campaign { get; internal set; }
        public ProfileSeason season { get; internal set; }
        public ProfileReward reward { get; internal set; }
        public ProfileAchievements achievements { get; internal set; }

        public Profile(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.realm = int.Parse(rawJson["realm"].ToString());
            this.displayName = rawJson["displayName"].ToString();
            this.clanName = rawJson["clanName"].ToString();
            this.clanTag = rawJson["clanTag"].ToString();
            this.profilePath = rawJson["profilePath"].ToString();
            this.portrait = new Icon(JObject.Parse(rawJson["portrait"].ToString()));
            this.career = new ProfileCareer(JObject.Parse(rawJson["career"].ToString()));
            this.swarmLevels = new ProfileSwarmLevels(JObject.Parse(rawJson["swarmLevels"].ToString()));
            this.campaign = new ProfileCampaign(JObject.Parse(rawJson["campaign"].ToString()));
            this.season = new ProfileSeason(JObject.Parse(rawJson["season"].ToString()));
            this.reward = new ProfileReward(JObject.Parse(rawJson["reward"].ToString()));
            this.achievements = new ProfileAchievements(JObject.Parse(rawJson["achievements"].ToString()));
        }
    }
}
