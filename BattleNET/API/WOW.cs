﻿using BattleNET.Objects.WOW;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.API
{
    public class WOW
    {
        private BattleNETClient parent;

        private string api_url;
        private string locale;
        private string api_key;
        private string user_agent;

        public WOW(BattleNETClient parent, string api_url, string locale, string api_key, string user_agent)
        {
            this.parent = parent;

            this.api_url = api_url;
            this.locale = locale;
            this.api_key = api_key;
            this.user_agent = user_agent;
        }

        #region ACHIEVEMENT API
        public Achievement getAchievement(int id)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/achievement/{id}?locale={locale}&apikey={api_key}");
            return new Achievement(JObject.Parse(request.response));
        }
        #endregion

        #region AUCTION API
        public List<Auction> getAuctions(string realm)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/auction/data/{realm}?locale={locale}&apikey={api_key}");
            request.GET(JObject.Parse(request.response)["files"][0]["url"].ToString());

            List<Auction> auctions = new List<Auction>();
            foreach (JObject JAuction in JObject.Parse(request.response)["auctions"])
                auctions.Add(new Auction(JAuction));
            return auctions;
        }
        #endregion

        #region BOSS API
        public List<Boss> getBossMasterList()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/boss/?locale={locale}&apikey={api_key}");

            List<Boss> bosses = new List<Boss>();
            foreach (JObject JBoss in JObject.Parse(request.response)["bosses"])
                bosses.Add(new Boss(JBoss));
            return bosses;
        }
        public Boss getBoss(int id)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/boss/{id}?locale={locale}&apikey={api_key}");
            return new Boss(JObject.Parse(request.response));
        }
        #endregion
        
        #region CHALLENGE MODE API
        public List<Challenge> getRealmLeaderboards(string realm)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/challenge/{realm}?locale={locale}&apikey={api_key}");

            List<Challenge> challenges = new List<Challenge>();
            foreach (JObject JChallenge in JObject.Parse(request.response)["challenge"])
                challenges.Add(new Challenge(JChallenge));

            return challenges;
        }
        public List<Challenge> getRegionLeaderboards()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/challenge/region?locale={locale}&apikey={api_key}");

            List<Challenge> challenges = new List<Challenge>();
            foreach (JObject JChallenge in JObject.Parse(request.response)["challenge"])
                challenges.Add(new Challenge(JChallenge));

            return challenges;
        }
        #endregion

        #region CHARACTER PROFILE API
        [Obsolete("You have to build your own field string by using the names in this enum seperated by comma")]
        public enum characterProfileField
        {
            achievements,
            appearance,
            feed,
            guild,
            hunterPets,
            items,
            mounts,
            pets,
            petSlots,
            progression,
            pvp,
            quests,
            reputation,
            statistics,
            stats,
            talents,
            titles,
            audit
        }
        public Character getCharacterProfile(string realm, string charactername)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/character/{realm}/{charactername}?locale={locale}&apikey={api_key}");
            return new Character(JObject.Parse(request.response));
        }
        public Character getCharacterProfile(string realm, string charactername, string fields)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/character/{realm}/{charactername}?fields={fields}&locale={locale}&apikey={api_key}");
            return new Character(JObject.Parse(request.response));
        }
        #endregion

        #region GUILD PROFILE API
        public Guild getGuildProfile(string realm, string guildname)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/guild/{realm}/{guildname}?locale={locale}&apikey={api_key}");
            return new Guild(JObject.Parse(request.response));
        }
        public Guild getGuildProfile(string realm, string guildname, string fields)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/guild/{realm}/{guildname}?fields={fields}&locale={locale}&apikey={api_key}");
            return new Guild(JObject.Parse(request.response));
        }
        #endregion

        #region ITEM API
        public Item getItem(int itemid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/item/{itemid}?locale={locale}&apikey={api_key}");
            return new Item(JObject.Parse(request.response));
        }
        public ItemSet getItemSet(int setid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/item/set/{setid}?locale={locale}&apikey={api_key}");
            return new ItemSet(JObject.Parse(request.response));
        }
        #endregion

        #region MOUNT API
        public List<Mount> getMountMasterList()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/mount/?locale={locale}&apikey={api_key}");
            JObject rawJson = JObject.Parse(request.response);
            List<Mount> mounts = new List<Mount>();
            foreach(JObject JMount in rawJson["mounts"])
            {
                Mount mount = new Mount(JMount);
                mounts.Add(mount);
            }
            return mounts;
        }
        #endregion

        #region PET API
        public List<Pet> getPetMasterList()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/pet/?locale={locale}&apikey={api_key}");
            JObject rawJson = JObject.Parse(request.response);
            List<Pet> pets = new List<Pet>();
            foreach (JObject JPet in rawJson["pets"])
            {
                Pet pet = new Pet(JPet);
                pets.Add(pet);
            }
            return pets;
        }
        public Pet.PetAbility getPetAbility(int abilityid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/pet/ability/{abilityid}?locale={locale}&apikey={api_key}");
            return new Pet.PetAbility(JObject.Parse(request.response));
        }
        public Pet.PetSpecies getPetSpecies(int speciesid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/pet/species/{speciesid}?locale={locale}&apikey={api_key}");
            return new Pet.PetSpecies(JObject.Parse(request.response));
        }
        public Pet.PetStats getPetStats(int speciesid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/pet/stats/{speciesid}?locale={locale}&apikey={api_key}");
            return new Pet.PetStats(JObject.Parse(request.response));
        }
        #endregion

        #region PVP API
        public enum pvpLeaderBoardBracket
        {
            v2,
            v3,
            v5,
            rbg
        }
        public List<PVP> getPVPLeaderboards(pvpLeaderBoardBracket bracket)
        {
            string bracketRight;
            switch (bracket)
            {
                case pvpLeaderBoardBracket.v2:
                    bracketRight = "2v2";
                    break;
                case pvpLeaderBoardBracket.v3:
                    bracketRight = "3v3";
                    break;
                case pvpLeaderBoardBracket.v5:
                    bracketRight = "5v5";
                    break;
                case pvpLeaderBoardBracket.rbg:
                    bracketRight = "rbg";
                    break;
                default:
                    throw new Exception($"The bracket [{bracket}] is not supported!");
            }
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/leaderboard/{bracketRight}?locale={locale}&apikey={api_key}");

            List<PVP> pvpLeaderBoard = new List<PVP>();
            foreach (JObject JPVP in JObject.Parse(request.response)["rows"])
                pvpLeaderBoard.Add(new PVP(JPVP));

            return pvpLeaderBoard;
        }
        #endregion

        #region QUEST API
        public Quest getQuest(int questid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/quest/{questid}?locale={locale}&apikey={api_key}");

            return new Quest(JObject.Parse(request.response));
        }
        #endregion

        #region REALM STATUS API
        public List<Realm> getRealmStatus()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/realm/status?locale={locale}&apikey={api_key}");
            List<Realm> realms = new List<Realm>();
            foreach (JObject JRealm in JObject.Parse(request.response)["realms"])
                realms.Add(new Realm(JRealm));

            return realms;
        }
        #endregion

        #region RECIPE API
        public Recipe getRecipe(int recipeid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/recipe/{recipeid}?locale={locale}&apikey={api_key}");

            return new Recipe(JObject.Parse(request.response));
        }
        #endregion

        #region SPELL API
        public Spell getSpell(int spellid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/spell/{spellid}?locale={locale}&apikey={api_key}");

            return new Spell(JObject.Parse(request.response));
        }
        #endregion

        #region ZONE API
        public List<Zone> getZoneMasterList()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/zone/?locale={locale}&apikey={api_key}");

            List<Zone> zones = new List<Zone>();
            foreach (JObject JZone in JObject.Parse(request.response)["zones"])
                zones.Add(new Zone(JZone));

            return zones;
        }

        public Zone getZone(int zoneid)
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/zone/{zoneid}?locale={locale}&apikey={api_key}");

            return new Zone(JObject.Parse(request.response));
        }
        #endregion


        // TODO
        #region DATA RESOURCES
        public List<Battlegroup> getBattlegroups()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/data/battlegroups/?locale={locale}&apikey={api_key}");

            List<Battlegroup> battlegroups = new List<Battlegroup>();
            foreach (JObject JBattlegroup in JObject.Parse(request.response)["battlegroups"])
                battlegroups.Add(new Battlegroup(JBattlegroup));

            return battlegroups;
        }
        public List<Race> getCharacterRaces()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/data/character/races?locale={locale}&apikey={api_key}");

            List<Race> races = new List<Race>();
            foreach (JObject JRace in JObject.Parse(request.response)["races"])
                races.Add(new Race(JRace));

            return races;
        }
        public List<Class> getCharacterClasses()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/data/character/classes?locale={locale}&apikey={api_key}");

            List<Class> classes = new List<Class>();
            foreach (JObject JClass in JObject.Parse(request.response)["classes"])
                classes.Add(new Class(JClass));

            return classes;
        }
        public List<Achievement> getCharacterAchievements()
        {
            Request request = new Request(this.user_agent);
            request.GET($"{api_url}wow/data/character/achievements?locale={locale}&apikey={api_key}");

            List<Achievement> achievements = new List<Achievement>();
            foreach (JObject JAchievementList in JObject.Parse(request.response)["achievements"])
                foreach (JObject JAchievement in JAchievementList["achievements"])
                    achievements.Add(new Achievement(JAchievement));

            return achievements;
        }
        #endregion
    }
}
