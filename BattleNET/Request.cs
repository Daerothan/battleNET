﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET
{
    class Request
    {
        public int status { get; internal set; }
        public string response { get; internal set; }
        public WebHeaderCollection header { get; internal set; }
        public Stream responseStream { get; internal set; }
        public string url { get; internal set; }
        private string user_agent;

        public Request(string user_agent)
        {
            this.user_agent = user_agent;
        }

        private void writeLog(string method, string file, string url, string response, WebHeaderCollection header)
        {
            using (var sw = new StreamWriter(file, true))
            {
                sw.WriteLine($"URL: {method.ToUpper()} [{this.status}] {url}");
                sw.WriteLine($"Body:");
                sw.WriteLine($"{this.response}");
                sw.WriteLine($"");
                sw.Flush();
                sw.Close();
            }
        }

        public async Task<int> GETStatusAsync(string url)
        {
            try
            {
                var httpRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                httpRequest.Method = "GET";
                httpRequest.UserAgent = user_agent;

                var httpResponse = (HttpWebResponse)await httpRequest.GetResponseAsync();

                return (int)httpResponse.StatusCode;
            }
            catch (WebException e) { return (int)e.Status; }
            catch (Exception) { return 503; }
        }

        public void GET(string url, WebHeaderCollection headers = null)
        {
            try
            {
                var httpRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                httpRequest.Method = "GET";
                httpRequest.UserAgent = user_agent;
                httpRequest.ContentType = "application/json";
                httpRequest.AllowAutoRedirect = false;

                if (headers != null)
                {
                    for (int i = 0; i < headers.Count; i++)
                    {
                        httpRequest.Headers.Add(headers.Keys[i], headers.Get(i));
                    }
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                WebHeaderCollection header = httpResponse.Headers;
                this.responseStream = httpResponse.GetResponseStream();

                using (var sr = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var response = sr.ReadToEnd();

                    this.status = (int)httpResponse.StatusCode;
                    this.response = response;
                    this.header = header;
                }
            }
            catch (WebException ex)
            {
                this.status = (int)ex.Status;
                this.response = $"{new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()}";
            }

            writeLog("get", "request_log.txt", url, response, header);
        }
        
        public void POST(string url, IDictionary<string, string> data, WebHeaderCollection headers = null)
        {
            try
            {
                var httpRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                httpRequest.Method = "POST";
                httpRequest.UserAgent = user_agent;
                httpRequest.ContentType = "application/json";

                if (headers != null)
                {
                    for (int i = 0; i < headers.Count; i++)
                    {
                        httpRequest.Headers.Add(headers.Keys[i], headers.Get(i));
                    }
                }

                string post = JsonConvert.SerializeObject(data);

                Console.WriteLine(post);

                using (var sw = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    sw.Write(post);
                    sw.Flush();
                    sw.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                WebHeaderCollection header = httpResponse.Headers;
                this.responseStream = httpResponse.GetResponseStream();

                using (var sr = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var response = sr.ReadToEnd();

                    this.status = (int)httpResponse.StatusCode;
                    this.response = response;
                    this.header = header;
                    this.url = httpResponse.ResponseUri.ToString();
                }
            }
            catch (WebException ex)
            {
                this.status = (int)ex.Status;
                this.response = $"{new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()}";
            }

            writeLog("post", "request_log.txt", url, response, header);
        }

        public void PATCH(string url, IDictionary<string, string> data, WebHeaderCollection headers = null)
        {
            try
            {
                var httpRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                httpRequest.Method = "PATCH";
                httpRequest.UserAgent = user_agent;
                httpRequest.ContentType = "application/json";

                if (headers != null)
                {
                    for (int i = 0; i < headers.Count; i++)
                    {
                        httpRequest.Headers.Add(headers.Keys[i], headers.Get(i));
                    }
                }

                string post = JsonConvert.SerializeObject(data);
                using (var sw = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    sw.Write(post);
                    sw.Flush();
                    sw.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                WebHeaderCollection header = httpResponse.Headers;
                this.responseStream = httpResponse.GetResponseStream();

                using (var sr = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var response = sr.ReadToEnd();

                    this.status = (int)httpResponse.StatusCode;
                    this.response = response;
                    this.header = header;
                }
            }
            catch (WebException ex)
            {
                this.status = (int)ex.Status;
                this.response = $"{new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()}";
            }

            writeLog("patch", "request_log.txt", url, response, header);
        }

        public void DELETE(string url, WebHeaderCollection headers = null)
        {
            try
            {
                var httpRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                httpRequest.Method = "DELETE";
                httpRequest.UserAgent = user_agent;
                httpRequest.ContentType = "application/json";

                if (headers != null)
                {
                    for (int i = 0; i < headers.Count; i++)
                    {
                        httpRequest.Headers.Add(headers.Keys[i], headers.Get(i));
                    }
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                WebHeaderCollection header = httpResponse.Headers;
                this.responseStream = httpResponse.GetResponseStream();

                using (var sr = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var response = sr.ReadToEnd();

                    this.status = (int)httpResponse.StatusCode;
                    this.response = response;
                    this.header = header;
                }
            }
            catch (WebException ex)
            {
                this.status = (int)ex.Status;
                this.response = $"{new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()}";
            }

            writeLog("delete", "request_log.txt", url, response, header);
        }

        public void PUT(string url, IDictionary<string, string> data, WebHeaderCollection headers = null)
        {
            try
            {
                var httpRequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                httpRequest.Method = "PUT";
                httpRequest.UserAgent = user_agent;
                httpRequest.ContentType = "application/json";

                if (headers != null)
                {
                    for (int i = 0; i < headers.Count; i++)
                    {
                        httpRequest.Headers.Add(headers.Keys[i], headers.Get(i));
                    }
                }

                string post = JsonConvert.SerializeObject(data);
                using (var sw = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    sw.Write(post);
                    sw.Flush();
                    sw.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                WebHeaderCollection header = httpResponse.Headers;
                this.responseStream = httpResponse.GetResponseStream();

                using (var sr = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var response = sr.ReadToEnd();

                    this.status = (int)httpResponse.StatusCode;
                    this.response = response;
                    this.header = header;
                }
            }
            catch (WebException ex)
            {
                this.status = (int)ex.Status;
                this.response = $"{new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()}";
            }

            writeLog("put", "request_log.txt", url, response, header);
        }
    }
}
